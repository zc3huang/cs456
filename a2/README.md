# Running the files
Make sure that we are in the correct directory, otherwise the input.txt file will not be recognized.

Set the file permissions
```
chmod +x ./network_emulator.py
chmod +x ./receiver.py
chmod +x ./sender.py
```

Now we should open 3 different hosts on the Linux CS Server.

Start the network emulator on host 1.

```
./network_emulator.py [port1] [host2] [port4] [port3] [host3] [port2] [max_delay] [drop_probability] [verbose]
```

Start the receiver on host 2.
```
./receiver.py [host1] [port3] [port4] output.txt
```

Start the sender on host 3.
```
./sender.py [host1] [port1] [port2] [timeout] input.txt
```

I have provided a sample input file named `input.txt`. It contains over 20000 bytes of Lorem Ipsum text, which is enough for the sequence numbers to loop back from 31 to 0 to test its full functionality.

# Testing

Using putty, I ssh'ed into the following servers for the respective programs:
- `ubuntu2204-002` for `network_emulator.py`
- `ubuntu2204-004` for `receiver.py`
- `ubuntu2204-006` for `sender.py`

To start each respective program, I used the following commands:

`./network_emulator.py 11001 ubuntu2204-004 11004 11003 ubuntu2204-006 11002 500 0.2 1`

`./receiver.py ubuntu2204-002 11003 11004 output.txt`

`./sender.py ubuntu2204-002 11001 11002 200 input.txt`

If some of these ports aren't available, I picked free ones by running: 

`comm -23 <(seq 1024 65535 | sort) <(ss -tan | awk '{print $4}' | cut -d':' -f2 | grep "[0-9]\{1,5\}" | sort -u) | shuf |
head -n 3`

Just make sure that each [port[x]] matches in the command line arguments

To check for correctness, I ran:

`diff -w -B input.txt output.txt`

There was something wrong with whitespace changes, so that's why `-w -B` is necessary. Opening up both these files they look identical by inspection.