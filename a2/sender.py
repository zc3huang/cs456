#!/usr/bin/env python3

import os, sys
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

import time
import threading
import argparse
import socket

from packet import Packet

class Sender:
    def __init__(self, ne_host, ne_port, port, timeout, send_file, seqnum_file, ack_file, n_file, send_sock: socket.socket, recv_sock: socket.socket):

        self.ne_host = ne_host
        self.ne_port = int(ne_port)
        self.port = int(port)
        self.timeout = int(timeout) / 1000 # needs to be in seconds

        self.send_file = send_file # file object holding the file to be sent
        self.seqnum_file = seqnum_file # seqnum.log
        self.ack_file = ack_file # ack.log
        self.n_file = n_file # N.log

        self.send_sock = send_sock
        self.recv_sock = recv_sock

        # internal state
        self.lock = threading.RLock() # prevent multiple threads from accessing the data simultaneously
        self.window = [] # unacked packets, sorted by send time
        self.window_size = 1 # Current window size 
        self.timer = None # Threading.Timer object that calls the on_timeout function
        self.timer_packet : Packet = None # The packet that is currently being timed, should be the first item in self.window
        self.current_time = 0 # Current 'timestamp' for logging purposes

        self.data = None
        self.length = -1
        self.seqnum = 0
        self.receivedEOT = False

    def run(self):
        self.recv_sock.bind(('', self.port))
        self.perform_handshake()

        # write initial N to log
        self.n_file.write('t={} {}\n'.format(self.current_time, self.window_size))
        self.current_time += 1

        recv_ack_thread = threading.Thread(target=sender.recv_ack)
        send_data_thread = threading.Thread(target=sender.send_data)
        recv_ack_thread.start()
        send_data_thread.start()
        
        recv_ack_thread.join()
        send_data_thread.join()
        exit()

    def perform_handshake(self):
        "Performs the connection establishment (stage 1) with the receiver"

        while True:
            # send SYN packet
            packet = Packet(3, 0, 0, "")
            self.send_sock.sendto(packet.encode(), (self.ne_host, self.ne_port))
            self.seqnum_file.write('t={} {}\n'.format(-1, "SYN"))
            self.recv_sock.settimeout(3.0)
            
            try: # try waiting for response
                received_packet, addr = self.recv_sock.recvfrom(2048)
                self.ack_file.write('t={} {}\n'.format(-1, "SYN"))
                type, seqnum, length, data = Packet(received_packet).decode()
                if type == 3: # received handshake packet back
                    break
            except socket.timeout: # keep trying if it times out
                continue
        
        self.recv_sock.settimeout(None)
        return True    

    def recv_ack(self):
        """
        Thread responsible for accepting acknowledgements and EOT sent from the network emulator.
        """
        
        while True:
            print("Thread to receive")
            if self.timer and (time.time() - self.timer) > self.timeout: # check if timeout
                print("timeout occurred")
                self.on_timeout()

            try: # try receiving packets
                self.recv_sock.settimeout(self.timeout)
                received_packet, addr = self.recv_sock.recvfrom(2048)
            except BlockingIOError: # received error
                pass
            except socket.timeout:
                continue
            if received_packet == None: # received error
                continue

            print("received packet")
            type, seqnum, length, data = Packet(received_packet).decode()
            self.ack_file.write('t={} {}\n'.format(self.current_time, seqnum if type != 2 else "EOT"))
            self.current_time += 1
            if type != 0: # not an ack received
                if type == 2: # received EOT packet, should close everything
                    self.send_sock.close()
                    self.recv_sock.close()
                    self.receivedEOT = True
                    break
                continue

            # check if it's a new ack (seqnum is in our expected window)
            potential_seqnums = []
            for packet in self.window:
                _, potential_seqnum, _, _ = packet.decode()
                potential_seqnums.append(potential_seqnum)

            try:
                i = potential_seqnums.index(seqnum)
            except Exception:
                print("not a new ack, ignore and try again to receive new packets")
                continue # not a new ack, ignore and try again to receive new packets
            
            print("received new ack")
            if self.window_size < 10:
                self.window_size += 1
            self.n_file.write('t={} {}\n'.format(self.current_time, self.window_size))
            self.window = self.window[(i + 1):] # cumultive ack acknowledges all sent packets up until i, so pop them all
            self.timer_packet = None if len(self.window) == 0 else self.window[0] # oldest sent packet is now first item in self.window
            self.timer = None if len(self.window) == 0 else time.time()

        return True

    def send_data(self):
        """ 
        Thread responsible for sending data and EOT to the network emulator.
        """
        
        while True:
            print("Thread to send")
            if len(self.window) < self.window_size: # can send packets
                if self.data == None: # does not have existing data that needs to be sent
                    print("reading new data")
                    self.data = self.send_file.read(500)
                    self.length = len(self.data)
                    print(f"read {self.length} bytes")

                if self.length == 0 and len(self.window) == 0: # send EOT
                    print("sending EOT")
                    packet = Packet(2, 0, 0, "")
                    self.send_sock.sendto(packet.encode(), (self.ne_host, self.ne_port))
                    self.seqnum_file.write('t={} {}\n'.format(self.current_time, "EOT"))
                    self.current_time += 1
                    time.sleep(3.0) # wait 3 seconds before trying to send EOT again
                    if self.receivedEOT:
                        break
                    continue
                elif self.length == 0 and len(self.window) != 0: # should sent EOT but we have unacked packets in window, wait until we ack all packets to send EOT packet
                    print("should sent EOT but we have unacked packets in window, wait until we ack all packets to send EOT packet")
                    time.sleep(0.1)
                    continue
                elif self.length > 0: # send data packet
                    print("sending data packet")
                    packet = Packet(1, self.seqnum, self.length, self.data)
                    self.send_sock.sendto(packet.encode(), (self.ne_host, self.ne_port))
                    self.seqnum_file.write('t={} {}\n'.format(self.current_time, self.seqnum))
                    
                    self.data = None # reset our temporary write buffer
                    self.length = -1
                    self.current_time += 1
                    self.seqnum = (self.seqnum + 1) % 32
                    self.window.append(packet) # add current packet to window of unacked packets

                    if self.timer == None:
                        self.timer = time.time()
                    self.timer_packet = self.window[0] # oldest sent packet is always first item in self.window
                    print("send complete")
            else: # send packet later
                print(f"window is full, sleeping. Current timeout duration is {(time.time() - self.timer)}. Max duration is {self.timeout}")
                time.sleep(0.1)
                continue

        return True

    def on_timeout(self):
        """
        Deals with the timeout condition
        """
        
        seqnum = self.timer_packet.seqnum
        
        self.window_size = 1
        self.n_file.write('t={} {}\n'.format(self.current_time, self.window_size))
        self.send_sock.sendto(self.timer_packet.encode(), (self.ne_host, self.ne_port)) # resend packet
        self.seqnum_file.write('t={} {}\n'.format(self.current_time, seqnum))
        self.timer_packet = self.window[0]
        self.timer = time.time() # reset timer

        self.current_time += 1
        return True

if __name__ == '__main__':
    # Parse args
    parser = argparse.ArgumentParser()
    parser.add_argument("ne_host", type=str, help="Host address of the network emulator")
    parser.add_argument("ne_port", type=int, help="UDP port number for the network emulator to receive data")
    parser.add_argument("port", type=int, help="UDP port for receiving ACKs from the network emulator")
    parser.add_argument("timeout", type=float, help="Sender timeout in milliseconds")
    parser.add_argument("filename", type=str, help="Name of file to transfer")
    args = parser.parse_args()

    # Clear the log files
    open('seqnum.log', 'w').close()
    open('ack.log', 'w').close()
    open('N.log', 'w').close()
   
    with open(args.filename, 'r') as send_file, open('seqnum.log', 'w') as seqnum_file, \
            open('ack.log', 'w') as ack_file, open('N.log', 'w') as n_file, \
            socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as send_sock, \
            socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as recv_sock:
        sender = Sender(args.ne_host, args.ne_port, args.port, args.timeout, 
            send_file, seqnum_file, ack_file, n_file, send_sock, recv_sock)
        sender.run()
