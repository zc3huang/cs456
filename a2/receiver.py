#!/usr/bin/env python3

import os
import sys
import argparse
import socket
import math

from packet import Packet

# Writes the received content to file
def append_to_file(filename, data):
    file = open(filename, 'a')
    file.write(data)
    file.close()

def append_to_log(packet: Packet):
    """
    Appends the packet information to the log file
    """
    type, seqnum, _, _ = Packet(packet).decode()
    if type == 3:
        seqnum = "SYN"
    elif type == 2:
        seqnum = "EOT"

    with open("arrival.log", 'a') as arrival_file:
        arrival_file.write("{}\n".format(seqnum))

    return True

def send_ack(packet : Packet): #Args to be added
    """
    Sends ACKs, EOTs, and SYN to the network emulator. and logs the seqnum.
    """

    s.sendto(packet.encode(), (args.ne_addr, int(args.ne_port)))
    print(f"sent packet {packet.seqnum} to network emulator")

    return True
    

if __name__ == '__main__':
    # Parse args
    parser = argparse.ArgumentParser(description="Congestion Controlled GBN Receiver")
    parser.add_argument("ne_addr", metavar="<NE hostname>", help="network emulator's network address")
    parser.add_argument("ne_port", metavar="<NE port number>", help="network emulator's UDP port number")
    parser.add_argument("recv_port", metavar="<Receiver port number>", help="network emulator's network address")
    parser.add_argument("dest_filename", metavar="<Destination Filename>", help="Filename to store received data")
    args = parser.parse_args()

    # Clear the output and log files
    open(args.dest_filename, 'w').close()
    open('arrival.log', 'w').close()

    expected_seq_num = 0 # Current Expected sequence number
    seq_size = 32 # Max sequence number
    max_window_size = 10 # Max number of packets to buffer
    recv_buffer = {}  # Buffer to store the received data, has format {seqnum, data}

    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as s:
        s.bind(('', int(args.recv_port)))  # Socket to receive data

        while True:
            # Receive packets, log the seqnum, and send response
            try: # try receiving packets
                print("trying to receive packets")
                received_packet, addr = s.recvfrom(2048)
            except BlockingIOError: # received error
                pass
            if received_packet == None: # received error
                continue

            type, seqnum, length, data = Packet(received_packet).decode()
            print("about to append to arrival.log")
            append_to_log(received_packet)
            if type == 3:
                print("send SYN packet")
                # send SYN back
                packet = Packet(3, 0, 0, "")
                send_ack(packet)
            elif type == 2:
                # send EOT back and terminate
                print("send EOT packet and terminate")
                packet = Packet(2, 0, 0, "")
                send_ack(packet)
                break
            else:
                # this is a data packet
                if seqnum == expected_seq_num: # this is the expected packet
                    print("received expected seqnum")
                    append_to_file(args.dest_filename, data)

                    expected_seq_num = (expected_seq_num + 1) % 32
                    while expected_seq_num in recv_buffer: # loop through all next seqnum packets which are in the buffer, and write them all to file
                        append_to_file(args.dest_filename, recv_buffer[expected_seq_num])
                        recv_buffer.pop(expected_seq_num)
                        expected_seq_num = (expected_seq_num + 1) % 32

                    send_ack(Packet(0, (expected_seq_num - 1) % 32, 0, ""))
                else: # this is not an expected packet
                    print("received unexpected seqnum")
                    potential_seqnums = set()
                    for i in range(10):
                        temp_seqnum = (expected_seq_num + 1) % 32
                        potential_seqnums.add(temp_seqnum)
                    
                    if seqnum in potential_seqnums: # if the packet is in the next 10 expected, save it to buffer
                        print("unexpected seqnum is in next 10, saving to buffer")
                        recv_buffer[seqnum] = data
                    else: # discard if it's not within the next 10
                        print("unexpected seqnum is not in next 10, discarding")

                    send_ack(Packet(0, (expected_seq_num - 1) % 32, 0, ""))