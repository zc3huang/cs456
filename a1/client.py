import socket
import sys

argumentCount = len(sys.argv)
arguments = sys.argv

if argumentCount < 5:
    print("Too few command line arguments, exiting")
    exit()


server_address = arguments[1]

# n_port needs to be a positive integer
n_port = arguments[2]
if not n_port.isnumeric() or (n_port.isnumeric and int(n_port) <= 0):
    print("Bad n_port, exiting")
    exit()
n_port = int(n_port)

# req_code needs to be an integer
req_code = arguments[3]
if not req_code.isnumeric():
    print("Bad req_code, exiting")
    exit()
msgs = arguments[4:]
msgs.append("EXIT")

res = []

# opens a tcp socket connection
try:
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((server_address, n_port))
        s.sendall(bytes(req_code, "utf-8"))
        data = s.recv(1024)

        # checks server response to sending the req_code
        if "incorrect code" == str(data):
            print("Sent incorrect code, exiting")
            exit()
        else:
            # server sends the generated r_port if the code is correct
            r_port = str(data, encoding='utf-8')
            s.close()

    # opens a udp socket specified by the server's response
    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as s:

        # sends as many messages as it can until it reaches server limit or runs out of messages
        for message in msgs:
            s.sendto(bytes(message, encoding="utf-8"), (server_address, int(r_port)))
            response = s.recvfrom(1024)[0].decode()

            if response == "LIMIT":
                res.append("Response limit reached")
                break
            res.append(response)
        s.close()
except:
    print("Error creating client connection, exiting")

# print client response
print(",".join(res))
exit()