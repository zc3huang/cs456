# Running the files
We must first start the server using `./server.sh <req_code> <req_lim>`, where
- <req_code> is an integer
- <req_lim> is a positive integer

Then, we start the client using `./client.sh <server_address> <n_port> <req_code> <msg_1> ... <msg_n>`, where
- <server_address> is an string
- <n_port> is a positive integer
- <req_code> is an integer
- <msg_i> is an string encapsulated by single quotes

Note that the <server_address> is the hostname of where the server is being run. To make this easier, I have printed out the hostname of the server after it runs along with the generated <n_port>. The <req_code> must be the same integer used to start the server.

I have tested this on my local machine, as well as on `ubuntu2204-002.student.cs.uwaterloo.ca` alongside `ubuntu2204-004.student.cs.uwaterloo.ca`

# Example run
We start the server on `ubuntu2204-002.student.cs.uwaterloo.ca` with `./server.sh 123 3`.

![Alt text](image.png)

It prints out its hostname which is `ubuntu2204-002`, and a SERVER_PORT of `56347`.

Keeping the server running, we open up a new terminal on `ubuntu2204-004.student.cs.uwaterloo.ca`.

We start the client with `./client.sh ubuntu2204-002 56347 123 'level' 'hello' 'A man, a plan, a canal: Panama' 'goose'`

![Alt text](image-1.png)

We obtain the client side response of `TRUE,FALSE,TRUE,Response limit reached`, as expected. Note that both the server and client gracefully exit.