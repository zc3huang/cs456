import socket
import sys

argumentCount = len(sys.argv)
arguments = sys.argv

if argumentCount < 3:
    print("Too few command line arguments, exiting")
    exit()

# finds a random free port for network communication
def find_free_port():
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.bind((socket.gethostname(), 0))            
        port = s.getsockname()[1]
        s.close()
        return int(str(port))

# sanitizes the string by removing non alphanumeric characters and turning them into lowercase
# checks if the subsequent string is a palindrome
def isPalindrome(s: str):
    s = ''.join(ch for ch in s if ch.isalnum())
    s = s.lower()
    reversed_s = ''.join(reversed(s))

    if (s == reversed_s):
        return True
    return False

# server address is 0.0.0.0 to allow connections from anywhere
server_address = "0.0.0.0"
n_port = find_free_port()

# req_code needs to be an integer
req_code = arguments[1]
if not req_code.isnumeric():
    print("Bad req_code, exiting")
    exit()

# req_lim needs to be a positive integer
req_lim = arguments[2]
if not req_lim.isnumeric() or (req_lim.isnumeric and int(req_lim) <= 0):
    print("Bad req_lim, exiting")
    exit()
req_lim = int(req_lim)

# opens a tcp socket connection
try:
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        print(socket.gethostname())
        print(f"SERVER_PORT={n_port}")
        s.bind((server_address, n_port))
        s.listen()
        conn, addr = s.accept()

        with conn:
            while True:
                data = conn.recv(1024)
                if not data:
                    break

                # checks for the req_code from the client
                if req_code == str(data, encoding='utf-8'):
                    print("Correct code")
                    r_port = find_free_port()
                    conn.sendall(bytes(str(r_port), "utf-8"))

                    # sends to the client a new random port, and binds a udp socket to listen to
                    with socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM) as s_udp:
                        s_udp.bind((server_address, r_port))

                        # receives client strings and checks if it's a palindrome
                        count = 0
                        while(count < req_lim):
                            bytesAddressPair = s_udp.recvfrom(1024)
                            message = bytesAddressPair[0].decode('utf-8')
                            address = bytesAddressPair[1]
                            print("Received message: ", message)
                            if message == "EXIT":
                                break

                            response = "TRUE" if isPalindrome(message) else "FALSE"
                            s_udp.sendto(bytes(response, encoding="utf-8"), address)
                            count += 1

                        try:
                            s_udp.sendto(bytes("LIMIT", encoding="utf-8"), address)
                        except:
                            pass
                else:
                    # closes connection if client enters incorrect code
                    print("Incorrect code, exiting")
                    print(str(data))
                    conn.sendall(b"incorrect code")
                    s.close()
except:
    print("Error creating server connection, exiting")

                