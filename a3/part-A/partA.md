# Add flow commands

Let's first describe the commands itself with its parameters.

- The first parameter denotes the switch that has the flow applied to it. E.g. s0 means that we are adding a rule to switch 0 in the topology.

Then we look for packets that match certain criteria.
- `in_port` matches the packets incoming on port #
- `ip` matches the protocol. In this case, we match packets that use `ip`
- `nw_src` matches the packets that have the source IP address
- `nw_dst` matches the packets that have the destination IP address

If a packet matches those conditions, the `actions` denotes what the switch should do with it.
- `mod_dl_src` modifies the source MAC address of the packet
- `mod_dl_dst` modifies the destination MAC address of the packet
- `output` denotes the egress port of the packet

Looking at the 4 rules, we see that for a packet travelling between h1 and h1, we pass through 2 switches. We must have 4 rules to specify the ingress and egress of a packet for each of the 2 switches.

For example, for 

```
$ofctl add-flow s0 \
in_port=1,ip,nw_src=10.0.0.2,nw_dst=10.0.1.2,actions=mod_dl_src:0A:00:0A:01:00:02,mod_dl_dst:0A:00:0A:FE:00:02,output=2
```

We see that we have a flow for s0. 
For packets matching a source IP address of `10.0.0.2` (h0) and a destination address of `10.0.1.2` (h1), they go through ingress port 1 (s0-eth1).
Hence, we should set the source MAC to `0A:00:0A:01:00:02` and the destionation MAC to `0A:00:0A:FE:00:02`, and it should go out on port 2 (s0-eth2), which is the correct path as on the topology.

The other rules are similar to this logic.