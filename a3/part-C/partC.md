# Part C

### 1)

![Alt text](1.png)

We note that when we ping h5 from h1, the packets go through the following path:

h1 -> s3 -> s2 -> s1 -> s5 -> s6 -> h5

When we initialize the ping, the controller must install the flow rules that we need so that the packets can pass through using that path.

First, the controller must get the IP address for the hosts as well as their MAC addresses for their switches (h1 <-> s3, h5 <-> s6). This is the `host_tracker:Learned ...` part.

Next, the controller installs the flow rules required, and we display the MAC addresses attached to the ports in the switches when packets are matched. There are 5 entries each for h1 and h5, and each entry is a rule, similar to the manual rules defined in part A/B. This is the `forwarding.l2_learning:installing flow for ...` part.

### 2)

![Alt text](2.png)

There is a big difference between the RTT of the first ping message with the subsequent ones. This is because it took some initial startup time for the controller to identify the IP/MAC addresses of the hosts, and to install the required flow rules to ensure that the packets could travel between each host.

### 3)

![Alt text](3_before.png)
Before pinging

![Alt text](3_after.png)
After pinging

The initial rules for all the switches are for connecting the switches to and from the controller.

After the ping, only the switches that are in the path of h1 to h5 have newly installed flow rules, namely s1, s2, s3, s5, s6. Since s4, s7 are not part of that path, they don't have any newly installed rules.

These OVS rules are slightly different from the rules defined in part A, because they don't have any pattern matching (e.g. checking source/destination IP). This indicates that it uses destination-based forwarding.
